#ifndef __STEPPER_H
#define __STEPPER_H

template<class VS>
// from VS we get the types PackedWord, Scalar, Element(packed vector)
struct stepper {
// To fill a packed vector destroying any current contents.
// 1. construct with ref to vector to be filled, packing data, eg pf = 3, pc = 21.
// 2. call step n times where n is the scalar  length of the vector.
// 3  call finish once to get the last partially filled word handled.
	// v will be written, pf is bits per scalar, pc is scalars per PackedWord.

	size_t _n; // length in scalars of packed vector to be filled by stepping.
	size_t _i; // count up to _n.
	size_t _pf; // bits per VS::Scalar
	size_t _pc; // VS::Scalar's per VS::PackedWord

	typename VS::PackedUnit _last; // currently being packed.

	size_t _lastcount; // available space in last. range 1 .. _pc

	typename VS::Element& _v; // vector being packed.
	
	stepper (typename VS::Element& v, size_t n, size_t pf = 3, size_t pc = 21) 
		: _n(n), _i(0), _pf(pf), _pc(pc), _last(0), _lastcount(pc), _v(v) {
		//std::cout << _v.size() << " started " << (long)&_v << std::endl;
		_v.clear();
	}
	inline void step(typename VS::Scalar e) { 
		//cout << " " << e;
		++_i;
		_last <<= _pf; 
		//_last |= static_cast<typename VS::PackedWord>(e); 
		_last |= e; 
		if (--_lastcount  == 0) {_v.push_back(_last); _last = 0; _lastcount = _pc;}
	}
	void finish() { 
		//cout << endl;
		// finish current PackedWord with 0's
		if (_lastcount < _pc) { 
			_i += _lastcount; 
			_v.push_back(_last <<_lastcount*_pf); 
			_last = 0;
		//	_lastcount = _pc; // for invariant, but not necessary because not used again
		}
		// then finish PackedVector with 0's
		while (_i < _n) { 
			_i += _pc; 
			_v.push_back(_last); 
		}
		// reset for invariant.  not necessary because not used (no restart)
		//_lastcount = _pc;
		//_i = 0;
		/*
		std::cout << _v.size() << " finished " << (long)&_v << std::endl;
		*/
	}

};

#endif // __STEPPER_H
