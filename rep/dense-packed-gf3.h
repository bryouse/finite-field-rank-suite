#ifndef __DENSE_PACKED_GF3_H
#define __DENSE_PACKED_GF3_H

#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <unistd.h>

// PackedUnit is LinBox::uint64.  There was an ambiguity prob.
//  octal rep of all "1" bits set e.g. 001001001...
#define MASK_1 (PackedUnit)0111111111111111111111
//  octal rep of all "2" bits set e.g. 010010010...
#define MASK_2 (PackedUnit)0222222222222222222222
//  octal rep of all "4" bits set. e.g. 100100100...
#define MASK_4 (PackedUnit)0444444444444444444444
//  octal rep of all "4" bits off e.g. 011011011...
#define MASK_4_INV (PackedUnit)0333333333333333333333

/**
  The matrix class Packed3 is defined.  
  It adheres to the LinBox dense matrix interface.
*/

#include "dense-matrix.h"

using namespace std;
using namespace LinBox;

/**
	The Packed3 Matrix class 
	_Domain must be  a GF(3) rep, BaseT must be an unsigned int type.
	TODO more docs, discuss row/col slicing
*/
template <class _Domain>
class Packed3 : public DenseMatrix<typename _Domain::Word_T>
{
public:
	typedef _Domain Domain;
	typedef typename Domain::Scalar Scalar;
	typedef typename Domain::Word_T PackedUnit;

	typedef DenseMatrix<typename _Domain::Word_T> Base_T;
	using Base_T::rawBegin;
	using Base_T::rawEnd;
	using Base_T::rowBegin;
	using Base_T::rowEnd;
	using Base_T::_stride;
	using Base_T::_rep;
	using Base_T::_alloc;
	typedef typename Base_T::RawIterator RawIterator;
	typedef Base_T Matrix;

	enum op_t { ADD, SMUL, AXPY, ZERO, COPY };   

	Packed3() : Matrix(), _domain(Domain()), _m(0), _n(0), _colPacked(false),
		_SIZE(8*sizeof(PackedUnit)), _sub(false) {}

	Packed3(Domain d) : Matrix(), _domain(d), _m(0), _n(0), _colPacked(false),
		_SIZE(8*sizeof(PackedUnit)), _sub(false) {}

	//  Constructor taking dims and bool defaulting ROWPACKED (3)
	Packed3 (Domain d, size_t m, size_t n, bool colP = false) :
		Matrix(), _domain(d), _colPacked(colP), _i(0), _j(0), _loff(0), 
		_roff(0), _SIZE(8*sizeof(PackedUnit)), _sub(0) {
			init(m, n);
			//std::cerr << "specialized GF3 packing" << std::endl;
	}

	//  TODO function pointer init vs size?
	//  need to override parent size, because we are a different size
	void size(size_t m = 0, size_t n = 0, bool cp = false){
		_m = m; _n = n; _p = 3; _colPacked = cp;
		_i = _j = _roff = _loff = _sub = 0;
		_valsPerWord = _SIZE/_p;
		_shifts = _valsPerWord - 1;
		_mask = (1 << _p) - 1;

 		if(_colPacked)
			Matrix::size((m + _valsPerWord - 1)/_valsPerWord, n);
		else
			Matrix::size(m, (n + _valsPerWord - 1)/_valsPerWord);
	}

	//  need to override parent inits, because we are a different size
	void init(size_t m = 0, size_t n = 0, size_t p = 3) {
		_m = m; _n = n; _p = 3;
		_i = _j = _roff = _loff = _sub = 0;
		_valsPerWord = _SIZE/_p;
		_shifts = _valsPerWord - 1;
		_mask = (1 << _p) - 1;

 		if(_colPacked)
			Matrix::init((m + _valsPerWord - 1)/_valsPerWord, n);
		else
			Matrix::init(m, (n + _valsPerWord - 1)/_valsPerWord);
	}

	void init_val(size_t m, size_t n, const Scalar &filler, size_t p = 3){
		init(m,n,p);

		//  TODO stepper functionality, more efficient.
		for(size_t i=0; i<m; ++i)
			for(size_t j=0; j<n; ++j)
				setEntry(i, j, filler);
	}

#ifdef __SUBMATRIX_IMPL__ // TODO
	Packed3 & submatrix(Packed3 &other, size_t i, size_t j, size_t m, size_t n){
		_m = m;
		_n = n;
		_colPacked = other._colPacked;
		_i = i;
		_sub = true;

		size_t headStart = other._loff ? _SIZE - other._loff : 0;
		_j = j + headStart;
		//  determine in which of parents' columns do we begin
		size_t firstCol =  _j/_SIZE;

		_loff = (_SIZE-(_j%_SIZE))%_SIZE;  // could prob. omit final mod?
		_roff = (_j+n)%_SIZE;
		
		// _roff really an offset? or just goes to "parent" matrix's normal end
		//if( (_j+n) == other.coldim() )
		//	_roff = 0;
			//  AND IF our super matrix is < SIZE, special case we don't account for
			*  ^ this can cause the writing of a superfluous zero word in s_write_bin
				if last word is next to first word in a submat *

		//std::cerr << _j << " " << n << other.coldim();
		//cerr << "LOFF: " << _loff << " ROFF: " << _roff << endl;
		//std::cerr << "ijmn?" << i << " " << j << " " << m << " " << n << std::endl;
		//cerr << "firstCol: " << firstCol << " _j: " << _j << " cols(): " << _cols << " other.cols(): " << other.cols() << endl;
		
		//TODO colpacked version
		this->Matrix::submatrix(other, i, firstCol, m, (n + (_j%_SIZE) + (_SIZE - 1))/_SIZE);

		return *this;
	}

	//  blindly assumes "other" matrix is rowpacked...
	Packed3 (Packed3 &other, size_t i, size_t j, size_t m, size_t n) : Matrix(), _domain(other._domain) { 
		//  we currently can't handle submatrices that reside entirely within
		//  a single sliced unit, width-wise...
		//  meaning < _SIZE columns, while both borders are in the same word
		size_t begin = j+other._loff;
		size_t end = begin+n;
		size_t endTest = other._n < _SIZE ? other._n : _SIZE;
		//  check if we're small enough, then check if we're on a border (which is OK)
		if(n < _SIZE && begin%_SIZE && end%endTest){
			size_t test = other._loff ? other._loff : _SIZE;
			if((j+n) <= test){
				cerr << "Unsupported submatrix request.  Submatrix entirely within a word" << 
					" while not aligned with a left or right border" <<	endl;
				//exit(-1);
			}
		}

		//  passed the security checkpoint.  on to business
		submatrix(other, i, j, m, n);
	}

// incl functions to handle specialized cases (where submats are not word-aligned)
#include "dense-sliced.inl"
#endif 

	Scalar& getEntry(Scalar &x, size_t i, size_t j){
		//  TODO:  see note in setEntry re: _rep for subs
		//  raw iterator way
		//RawIterator word = rowBegin(i) + w;
		//int w = ((j+(_j%_SIZE))/_SIZE);
		//word += w;

		size_t word = (_i+i)*_stride + ((j+(_j%_valsPerWord))/_valsPerWord);
		size_t index = (_j+j) % _valsPerWord;
		size_t shiftwidth = ((_shifts-index)*_p);
		size_t answer = (size_t)((_rep[word] >> shiftwidth) & _mask);
		//cerr << "(w" << w << ",i" << index << ")";
		return _domain.init(x, answer);
	}

	//TODO: can specialize for a known-zero entry
	void setEntry(size_t i, size_t j, const Scalar &a_ij){
		PackedUnit e = static_cast<PackedUnit>(a_ij);
		//  determine location
		//  TODO:  THIS SEEMS TO EXCPECT _rep isn't adjusted for submatrix
		//  whereas in dense-matrix.h, submatrix adjusts _rep.  need to study 
		//  the pros/cons of either approach but CANNOT MIX THEM
		//  raw iterator way
		//int w = ((j+(_j%_SIZE))/_SIZE);
		//RawIterator word = rowBegin(i) + w;
		//word += w;
		size_t word = (_i+i)*_stride + ((j+(_j%_valsPerWord))/_valsPerWord);
		size_t index = (_j+j) % _valsPerWord;
		size_t shiftwidth = ((_shifts-index)*_p);

		_rep[word] &= ~(_mask << shiftwidth);  // zero old
		_rep[word] |= e << shiftwidth;  // OR in new
	}

	inline PackedUnit semiNormalValueBase(PackedUnit &in){
		PackedUnit temp = in & static_cast<PackedUnit>(MASK_4);
		in &= static_cast<PackedUnit>(MASK_4_INV);
		return temp >> 2;
	}
	inline PackedUnit & semiNormalValueAdd(PackedUnit &in){
		return in += semiNormalValueBase(in);
	}
	inline PackedUnit & semiNormalValueMul(PackedUnit &in){
		return in |= semiNormalValueBase(in);
	}

	/* other half of the half-normalization.  the only thing
		this has to do is turn all of the 3's into 0's */
	inline PackedUnit & finishNormalValue(PackedUnit &in){
		PackedUnit one, two, temp;
		// loop over each packed value
		one = in & MASK_1;
		two = in & MASK_2;
		//  check for bit overlap when shifting adjacent values
		temp = (two & one << 1) | (one & two >> 1);
		//  zero out any bit overlap
		return in &= ~temp;
	}

	// TODO pass function object directly rather than truth testing?
	Packed3 & semiNormalize(bool Mul=false){
		for(RawIterator a = rawBegin(); a != rawEnd(); ++a)
			if(Mul)
				semiNormalValueMul(*a);
			else
				semiNormalValueAdd(*a);
	}

	//  this can linkely be used at all times 
	//  for a final normalize call
	//  if arithmetic forces seminormaliation (as currently does)
	Packed3 & finishNormalize(){
		for(RawIterator a = rawBegin(); a != rawEnd(); ++a)
			finishNormalValue(*a);
	}

	//  likely not needed
	Packed3 & normalize(){
		for(RawIterator a = rawBegin(); a != rawEnd(); ++a){
			semiNormalValueAdd(*a);
			finishNormalValue(*a);
		}
	}

	//  typical addin, barge right through with the raw iterator
	Packed3& addin(Packed3 &other){
		RawIterator a = rawBegin();
		RawIterator c = rawEnd();
		RawIterator b = other.rawBegin();
		for(; a != c; ++a, ++b)
			semiNormalValueAdd((*a) += (*b));
			//(*a) += (*b);
		return *this;
	}

	Packed3& smulin(Scalar &x){
		if(x == 1) return *this;
		//if(_loff || _roff) return s_smulin(x);
		if(x == 0) return zero();
		//  x == 2
		for(RawIterator a = rawBegin(); a != rawEnd(); ++a)
			semiNormalValueMul((*a) <<= 1);

		return *this;
	}

	Packed3 & axpyin(Scalar &a, Packed3 &x){
		RawIterator b(rawBegin()), e(rawEnd()), ob(x.rawBegin());
		return axpyin(b, e, a, ob);
	}

	/*
	Packed3& smul(Scalar &x){
		return Packed3(*this).copy(*this).smulin(x);
	}
	*/
	//  begin, end, scalar, other begin
	Packed3 & axpyin(RawIterator &b, RawIterator &e, Scalar &s, RawIterator &ob){
		RawIterator x = b;
		RawIterator y = ob; 
		/*
		//  TODO: fix this (see if it's even fixable)
		if(x == y){ // same matrix
			Scalar t; _domain.init(t, 1);
			_domain.addin(t, s);
			return smulin(t);
		}
		*/
		switch(static_cast<int>(s)){
			case 0:
				return *this;
			case 1: 
				//SUB if(_loff || _roff) return s_axpyin(b, s, ob);
				for(; x!=e; ++x,++y)
					semiNormalValueAdd((*x) += (*y));
				return *this;
			case 2:
				//SUB if(_loff || _roff) return s_axpyin(b, s, ob);
				for(; x!=e; ++x,++y){ 
					semiNormalValueAdd((*x) += semiNormalValueMul((*y) <<= 1));
				}
				return *this;
		}
		//  this shouldn't be reached (throw error?)
		std::cerr << "Problem in Packed3::axpyin" << std::endl;
		return *this;
	}

	//  MUL:
	//  become the product of two sliced matrices
	//  (only seems to work if row packed so far)
	//  (does not check for compatible sizes)
	//  (does NOT work yet for two submatrices)
	template <class Gettable>
	Packed3 & mul(Gettable& A, Packed3& B){
		zero();

		Scalar a_ij;
		//  c&b - begin and end
		RawIterator c_b, c_e, b_b; //, b_e;

		size_t count = 0;
		//  axpyin to C individual entries of A with rows of B
		for(; count < rowdim(); ++count){
			c_b = rowBegin(count);
			c_e = rowEnd(count);

			//Timer t;
			//t.clear(); t.start();
			for(size_t len = 0; len < A.coldim(); ++len){
				//  element of A goes down rows of A.  
				//  (could improve on speed of method to get this value [step?])
				a_ij = A.getEntry(a_ij, count, len);

				//  this is axpy'd to THAT row of B.
				b_b = B.rowBegin(len);
				axpyin(c_b, c_e, a_ij, b_b);
			}
			//t.stop();  std::cerr << t << std::endl;
		}
		return *this;
	}

	std::ostream& writeRep(std::ostream &os = std::cerr){
		for(RawIterator a = rawBegin(); a != rawEnd(); ++a){
			for(int i = _SIZE-1; i >=0; --i){
				os << (((*a) >> i) & 1);
			}
			os << " ";
		}
		return os << std::endl;
	}

	std::ostream& write(std::ostream &os = std::cerr){
		Scalar t;
		for(size_t i = 0; i<_m; i++){
			for(size_t j = 0; j<_n; j++)
				os << (size_t)getEntry(t, i, j) << " ";
			os << endl;
		}
		os << endl;
		return os;
	}

	/*
	// super slow random
	Packed3& random(size_t seed = 0){
		if(seed) srand(seed);

		for(size_t i = 0; i<_m; ++i)
			for(size_t j = 0; j<_n; ++j)
				setEntry(i, j, rand()%3);  //TODO field cardinality

		return *this;
	}
	*/

	//  returns an (approximately) random _SIZE-bit word
	PackedUnit random_unit(){
		PackedUnit r = 0;
		for(size_t i=0; i<(_SIZE/8); i++)
			r |= ((PackedUnit)(rand()%256) << (i << 3));
		return r;
	}

	//  completely randomizes sliced block entries
	//  faster than setEntry
	Packed3& random(size_t seed = 0){
		if(seed) srand(seed);

		for(RawIterator a = rawBegin(); a != rawEnd(); ++a){
			//  raw random might give us "sevens"
			//  which SNVA turns into "fours"
			//  which SNVM turns into "ones"
			finishNormalValue(
				semiNormalValueMul(
					semiNormalValueAdd((*a) = random_unit())));

		}
		return *this;
	}

	//  completely zeros out a sliced block
	Packed3& zero(){
		/* SUB
		if(_sub && (_loff || _roff)){
			Scalar t;
			//  TODO (can do [[[slightly]]] better if we specialize for 0
			return s_smulin(_domain.init(t,0));
		}
		*/
		memset( _rep, 0, memSize());
		return *this;
	}

	bool isEqual(Packed3 &other){
        if (rows() != other.rows() || cols() != other.cols()) {
			//std::cout << "shape mismatch" << std::endl; 
			return false;
		}
		return not memcmp(_rep, other._rep, memSize());
#if 0  //  this code looks pretty bad 
	//	if(_loff || _roff || other._loff || other._roff){
			//  very slow... could be improved if both mats are aligned
			Scalar x, y;
			for(size_t i = 0; i<rows(); ++i)
				for(size_t j = 0; j<cols(); ++j){
					//std::cerr << getEntry(x, i, j) << "vs" << other.getEntry(y, i, j) << std::endl;
					if(getEntry(x, i, j) != other.getEntry(y, i, j)) {
						//std::cout << "entry mismatch " << i << " " << j << std::endl; 
						return false;
					}
				}
	*  fails for very small (1 by 1) matrix.
		}
		else{
			//  otherwise just check _SIZE-bits at a time
			//  TODO could be faster w/ memcmp, but not applicable for breaks "aligned" submats
			for(RawIterator a = rawBegin(), b=other.rawBegin(); a != rawEnd(); ++a, ++b){
				//int count = 0;
				if((*a) != (*b)){
					//cerr << (*a).b0 << "ver" << (*b).b0 << endl;
					//cerr << (*a).b1 << "ver" << (*b).b1 << endl;
					return false;
				}
			}
		}
	*
		return true;
#endif
	}

#if 0 // needed?  if so a mem* function probably works a lot better
	bool isZero(){
		if(_loff || _roff){
			Scalar x;
			for(size_t i = 0; i<rows(); ++i)
				for(size_t j = 0; j<cols(); ++j)
					if(getEntry(x, i, j))
						return false;
		}
		else{
			//  otherwise just check _SIZE-bits at a time
			//  could prob be faster
			for(RawIterator a = rawBegin(); a != rawEnd(); ++a){
				if((*a).b0 || (*a).b1)
					return false;
			}
		}
		return true;
	}
#endif

	//  what the heck?
	Packed3& deepcopy(Packed3 &other){
		if(_sub) return s_copy(other);

		*this = other;
		return *this;
	}

	//  TODO assumes rowpack
	inline size_t memSize(){
		size_t v = _valsPerWord;
		size_t bytesPerRow = ((_n + v-1)/v) * sizeof(PackedUnit);
		return bytesPerRow  * _m;
	}

//  file r&w suite, can probably be a template class to write 
//  all things inheriting from dense-matrix (i.e. has _rep)
//  to and from files rather than repeating this effort
#if 0  
	std::ostream& writeBinary(std::ostream &os){
		if(_sub){ //std::cerr << "\n\nNear death!\n\n" << std::endl; 
			return s_wb(os); } // TODO fix

		size_t bytes = memSize();
		//std::cerr << "WRITE " << bytes << " BYTES." << std::endl;
		os.write((char *)&(*_rep), bytes);
		
		return os;
	}

	//  assumes we're NOT a submatrix (read into contiguous block)
	std::istream& readBinary(std::istream& is){
		if(_sub){ //std::cerr << "\n\nDeath!\n\n" << std::endl;
			return s_rb(is); }
		size_t bytes = memSize();
		is.read((char *)&(*_rep), bytes);
		// std::cerr << "READ " << bytes << " BYTES." << std::endl;
		return is;
	}

	bool writeBinaryFile(const char *file){
		ofstream bin;
		bin.open(file, ios::out | ios::binary);
		if(!bin){ std::cerr << "failure opening " << file << std::endl; return false; }
		writeBinary(bin);
		bin.close();
		return true;
	}

	bool readBinaryFile(const char *file){
		ifstream in;
		in.open(file, ios::in | ios::binary);
		if(!in){ std::cerr << "failure opening " << file << std::endl; return false; }
		readBinary(in);
		in.close();
		return true;
	}

    void mmapFile(int fd){
		if(_alloc){
			std::cerr << "alloc'd matrix trying to mmap." << std::endl;
			close(fd);
			return;
		}
        size_t bytes = memSize();

        //_rep = (Packed3::PackedUnit *)mmap(0, bytes, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
		//TODO MAP_HUGETLB
        _rep = (Packed3::PackedUnit *)mmap(0, bytes, PROT_READ | PROT_WRITE, MAP_SHARED , fd, 0);
        if((void *)_rep == MAP_FAILED) {
            close(fd);
            perror("Error mapping the file.");
        }
    }

    void mmapBinaryFile(const char *file){
        int fd = open(file, O_RDWR);
        if(fd == -1) {
            perror("Error opening file to mmap.");
        }
		mmapFile(fd);
    }

    void munmapBinaryFile(){
        size_t bytes = memSize();
        munmap((void *)_rep, bytes);
    }
#endif

	_Domain& domain(){ return _domain; }

	size_t rows(){ return _m; }
	size_t cols(){ return _n; }
	size_t rowdim(){ return _m; }
	size_t coldim(){ return _n; }

	size_t l(){ return _loff; }
	size_t r(){ return _roff; }

	//  pointer/offset info for debugging
	void pinfo(){
		std::cerr << "Matrix @ "; 
		RawIterator i = rawBegin();	
		i.pinfo();
		std::cerr << "\t" << _m << " x " << _n << std::endl;
		std::cerr << "\tLO: " << _loff << " RO: " << _roff << std::endl;
		std::cerr << std::endl;
	}
		
	//  size info for debugging
	void sinfo(){
		std::cerr << "Matrix " << _m << " by " << _n << std::endl;
		//std::cerr << "... " << _m * _n / 4 << " bytes." << std::endl;
	}

	size_t normalize_count(){
		return _nc;
	}

	size_t vpw(){
		return _valsPerWord;
	}

private:
	_Domain _domain;
	size_t _m, _n;
	size_t _p;
	bool _colPacked;
	//  SUBMATRIX
	size_t _i, _j;
	size_t _loff, _roff; //left & right offsets
	size_t _SIZE;
	bool _sub;
	size_t _valsPerWord, _shifts;
	PackedUnit _mask;
	size_t _nc;
}; // Sliced

#endif // __DENSE_SLICED_H
