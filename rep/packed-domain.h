#ifndef __PACKED_DOMAIN_H
#define __PACKED_DOMAIN_H

//  default matrix class
#include "dense-packed.h"

//TODO docs
//
template<class _Field,  typename _WordT = unsigned long long int, template<class> class _Packed = Packed>
struct PackedDomain : public _Field
{
	typedef _Field Field;
	typedef _WordT Word_T;
	typedef typename _Field::Element Element;
	typedef Element Scalar;
	typedef _Packed<PackedDomain> Matrix;

	//  TODO bad to get GF3 if unspecific
	PackedDomain () : Field(3) {}
	PackedDomain (size_t p) : Field(p) { }

	PackedDomain& operator=(const PackedDomain<Field, Word_T, _Packed>& R) 
	{ static_cast<Field*>(this)->operator= (R); return *this; }

// A domain provides field functions and (dense) matrix functions.
// field functions
	using Field::mOne;
	using Field::zero;
	using Field::one;
	using Field::init;
	using Field::add;	
	using Field::sub;	
	using Field::neg;	
	using Field::mul;	
	using Field::div;	
	using Field::inv;	
	using Field::axpy;	
	using Field::addin;	
	using Field::subin;	
	using Field::negin;	
	using Field::mulin;	
	using Field::divin;	
	using Field::invin;	
	using Field::axpyin;	
	using Field::isZero;	
	using Field::isOne;	
	using Field::areEqual;	
	using Field::cardinality;
	using Field::characteristic;
	using Field::write;
	using Field::read;

// matrix functions

	// Y += X, where X and Y are conformally packed (both row or both col).
	Matrix& addin (Matrix& Y, Matrix& X) { 
		return Y.addin(X);
	}	

	// X *= a
	Matrix& smulin (Matrix& X, Scalar& a) { 
		return X.smulin(a);
	}

#if 0
	// X -= X
	Matrix& neg (Matrix& X) { 
		return X.smulin(mOne);
	}
#endif

	//  return C <-- A * B
	//  is assuming row sliced
	template <class Gettable> // any matrix rep with getEntry().
	Matrix & mul (Matrix& C, Gettable& A, Matrix& B){
		return C.mul(A,B);
	}

#if 0
	//  this is really a specialization, would be better to provide the common interface
	//  which wraps this from within the matrix class itself
	//  TODO remove this
	// A += x*B
	Matrix& axpyin( Matrix& A, Scalar& x, Matrix &B) {
		typename Matrix::RawIterator Ab(A.rawBegin()), Ae(A.rawEnd()), Bb(B.rawBegin());
		return A.axpyin(Ab, Ae, x, Bb);
	}
#endif

	//  C += A * B
	Matrix& axpyin(Matrix& C, Matrix& A, Matrix &B) {
		//  temp mat to store mul
		Matrix T;
		T.init(A.rowdim(), B.coldim());
		mul(T, A, B);
		return addin(C, T);	
	}	

	// A += x*B
	Matrix& axpyin( Matrix& A, Scalar& x, Matrix &B) {
		return A.axpyin(x, B);
	}

	Matrix& random(Matrix &A, size_t seed=0) const {
		return A.random(seed);
	}

	Matrix& clear(Matrix &A) const {
		return A.zero();
	}

	// TODO this probably doesn't work right
	// assignment operator will not be a deep copy
	Matrix& deepcopy(Matrix &dst, Matrix& src) const {
		return dst.deepcopy(src);
	}

#ifdef __SUBMATRIX_IMPL__
	// submatrix is a Matrix member function, not here.
	Matrix& submatrix(Matrix& super, size_t i, size_t j, size_t m, size_t n) const {
		return *(new Matrix(super, i, j, m, n));
	}
#endif

	bool areEqual(Matrix& A, Matrix &B) const {
		return A.isEqual(B);
	}

	std::ostream& write(std::ostream& os, Matrix& A) const { 
		return A.write(os << A.rowdim() << " " << A.coldim() << std::endl); 
	}
	std::istream& read(std::istream& is, Matrix& A) const { 
		size_t r, c;
		Element x; init(x);
		is >> r >> c;
		A.init(r, c);
		for (size_t i = 0; i < r; ++i)
			for (size_t j = 0; j < c; ++j){
				read(is, x);
				A.setEntry(i, j, x);
			}
		return is; 
	}
};
	
#endif // __PACKD_DOMAIN_H
