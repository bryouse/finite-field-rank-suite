#ifndef __PLAIN_DOMAIN_H
#define __PLAIN_DOMAIN_H

#include <iostream>
#include "dense-matrix.h"

namespace LinBox {
/*
   PlainDomain is a reference implementation of a matrix domain.
   It makes a domain from any field by contributing very basic matrix functions using 
   the getEntry/setEntry functions of DenseMatrix 

PlainDomain has a constructor from field,
it's PlainDomain::Matrix subtype meets the dense matrix concept
and it is the interface for working with dense matrices

A Blackbox over PlainDomain<Field> is any matrix type that has apply and applyTranspose applicable to PlainDomain<Field>::Matrix.  That is to say the signature of apply is 
  Matrix& Blackbox::apply(Matrix& Y, const Matrix& X)

PlainDomain provides, for Matrices A,B,C
  mul(C, A, B) // C = A * B
  axpyin(C, A, B) // C += A * B
  addin(A, B) // A += B

  matrix functions:
  add sub neg mul div inv axpy (both scalar and matrix
  inplace forms of those
  areEqual clear, random, submatrix, deepcopy

  alternative functions
  gemm (rplace axpy) (trans)
  additional functions
  trsm (trans, uplo) trmm(trans, uplo) symm (trans, uplo)
  (in particular triangular solving must be included
*/

template<class _Field>
struct PlainDomain: public _Field //: public FieldInterface
{
	typedef _Field Field;
	// A domain provides distinct types: Element, Matrix, and Blackbox.
	typedef typename Field::Element Element;
	typedef Element Scalar;
	typedef typename LinBox::DenseMatrix<Scalar> Matrix;
	// but matrix should admit triangular forms

	PlainDomain (size_t p = 0, size_t e = 1): Field(p) {}
	PlainDomain& operator=(const PlainDomain<Field>& R) 
	{ static_cast<Field*>(this)->operator= (R); return *this; }

// A domain provides field functions and (dense) matrix functions.

// field functions
	using Field::init;
	using Field::add;	
	using Field::sub;	
	using Field::neg;	
	using Field::mul;	
	using Field::div;	
	using Field::inv;	
	using Field::axpy;	
	using Field::addin;	
	using Field::subin;	
	using Field::negin;	
	using Field::mulin;	
	using Field::divin;	
	using Field::invin;	
	using Field::axpyin;	
	using Field::isZero;	
	using Field::isOne;	
	using Field::areEqual;	
	using Field::cardinality;
	using Field::characteristic;
	using Field::write;
	using Field::read;

// matrix functions

	// C = A + B, where A, B, C have identical shape.
	Matrix& add (Matrix& C, const Matrix& B, const Matrix& A) { // crude
		Scalar a, b, c; 
		for (size_t i = 0; i < C.rowdim(); ++i)
			for (size_t j = 0; j < C.coldim(); ++j) {
				Field::add(c, B.getEntry(b, i, j), A.getEntry(a, i, j));
				C.setEntry(i, j, c);
			}	
		return C;
	}	

	// B += A, where B and A have the same shape.
	Matrix& addin (Matrix& B, const Matrix& A) 
		{  return add(B, B, A); }

	//  return C <-- A * B
	Matrix & mul (Matrix& C, const Matrix& A, const Matrix& B){
		Scalar c, a, b; 
		for (size_t i = 0; i < C.rowdim(); ++i)
			for (size_t j = 0; j < C.coldim(); ++j) {
				init(c, 0);
				for (size_t k = 0; k < A.coldim(); ++k) {
					axpyin(c, A.getEntry(a, i, k), B.getEntry(b, k, j));
				}
				C.setEntry(i, j, c);
			}	
		return C;
	}

	// C += A*B
	Matrix& axpyin( Matrix& C, const Matrix& A, const Matrix &B){
		Scalar c, a, b, x; 
		for (size_t i = 0; i < C.rowdim(); ++i)
			for (size_t j = 0; j < C.coldim(); ++j) {
				init(x, 0);
				for (size_t k = 0; k < A.coldim(); ++k) 
					axpyin(x, A.getEntry(a, i, k), B.getEntry(b, k, j));
				addin(x, C.getEntry(c, i, j));
				C.setEntry(i, j, x);
			}	
		return C;
	}

#if 0
	// Z = a*X + Y
	Matrix& axpy( Matrix& Z, const Scalar& a, const Matrix &X, const Matrix& Y) {
		Scalar z, x, y; 
		for (size_t i = 0; i < Z.rowdim(); ++i)
			for (size_t j = 0; j < Z.coldim(); ++j) {
				Field::mul(z, a, X.getEntry(x, i, j));
				Field::addin(z, Y.getEntry(y, i, j));
				Z.setEntry(i, j, z);
			}	
		return Z;
	}
	// Y += a*X
	Matrix& axpyin( Matrix& Y, const Scalar& a, const Matrix &X){ return axpy(Y, a, X, Y); }

#endif

	Matrix& clear(Matrix &A){
		Scalar a = 0; 
		for (size_t i = 0; i < A.rowdim(); ++i)
			for (size_t j = 0; j < A.coldim(); ++j)
				A.setEntry(i, j, a);
		return A;
	}

	Matrix& random(Matrix &A, size_t seed=0){
		//typename Field::RandIter r(*this, 2, 3); // arg?
		Scalar a; 
		for (size_t i = 0; i < A.rowdim(); ++i)
			for (size_t j = 0; j < A.coldim(); ++j) {
				//r.random(a);
				//A.setEntry(i, j, a);
				A.setEntry(i, j, rand()%3);
			}
		return A;
	}

	// assignment operator will be a deep copy
	Matrix& deepcopy(Matrix &dst, Matrix& src){
		return dst.deepcopy(src);
	}

	// this should not allocate mem.
	Matrix& submatrix(Matrix& super, size_t i, size_t j, size_t m, size_t n){
		return *(new Matrix(super, i, j, m, n));
	}

	bool areEqual(Matrix& A, Matrix &B){
		Scalar a, b; 
		for (size_t i = 0; i < A.rowdim(); ++i)
			for (size_t j = 0; j < A.coldim(); ++j) {
				A.getEntry(a, i, j); 
				B.getEntry(b, i, j);
				if (not _Field::areEqual(a, b) ) 
					return false;
			}
		return true;
	}

// simple write
	std::ostream& write(std::ostream& out, const Matrix& A){
		Scalar a; 
		//out << "%%MatrixMarketExtended array" << std::endl;
		out << A.rowdim() << " " << A.coldim() << std::endl;
		for (size_t i = 0; i < A.rowdim(); ++i){
			for (size_t j = 0; j < A.coldim(); ++j)
				write(out, A.getEntry(a, i, j)) << " "; 
			out << std::endl;
		}
		return out << std::endl;
	}

// simple read.  should use matrix reader
	std::istream& read(std::istream& in, Matrix& A){
		Scalar a; 
		size_t r, c;
		in >> r >> c;
		A.init(r, c);
		for (size_t i = 0; i < A.rowdim(); ++i)
			for (size_t j = 0; j < A.coldim(); ++j){
				read(in, a);
				A.setEntry(i, j, a);
			}
		return in;
	}

}; // PlainDomain
	
} // LinBox
#endif // __PLAIN_DOMAIN_H
