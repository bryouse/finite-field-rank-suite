import sys
from sys import argv,exit
import pickle
import datetime
import Queue
from threading import Thread,Lock
from math import ceil
from os import path
from time import sleep
from subprocess import call
from signal import signal,SIGUSR1

from dimensions import getb,getn #needs parent dir in PYTHONPATH

# SETUP GLOBAL CONSTANTS
try:
    e = int(argv[1])
    workers = 3
    if len(argv) > 2:
        workers = int(argv[2])
except:
    print "Usage",argv[0],"e [workers]"
    exit()

n = getn(e)
b = getb(e)
c = 64
dim = ((b+c)/c)*c
di=str(dim)
nblocks = ceil(float(n)/b)
rs = dim / 4
filesize = rs * dim
Go = True
add_queue = Queue.Queue()
printlock = Lock()
args = ['srun', '-p', 'all', '-N', '1', '-c', '48', './add-two-cols', str(e), 'J', 'OUTFILE']
#args = ['./add-two-cols', str(e), 'J', 'OUTFILE']
rmargs = ['rm', '']
consumed = None

#  threadsafe printing
class SafePrint(object):
    def __init__(self):
        self.terminal = sys.stdout

    def write(self, message):
        printlock.acquire()
        self.terminal.write(message)
        printlock.release()
sys.stdout = SafePrint()

# function defs
def fname(e=e):
    return "/usa/youse/checkpoint/cols_" + str(e)

def dfname(e=e):
    return "/usa/youse/checkpoint/done_" + str(e)

def load_dict(fn):
    #  deal with possible race condition for this file
    while True:
        try:
            with open(fn(), 'r+b') as f:
                d = pickle.load(f)
                break
        except EOFError as e:
            print e
            print "Trying to read %s again in 5 seconds..." % fn()
            sleep(5)

    print "Dictionary loaded from " + fn()
    return d

def dump_dict(d):
    with open(dfname(), 'w+b') as f:
        pickle.dump(d, f)
    print "Dictionary pickled to " + dfname()

def new_dict(n,b,init=0,dump=True):
    print "Creating new dictionary"
    d = {}
    for j in xrange(0, n, b):
        d[j] = init
    if dump:
        dump_dict(d)
    return d

def stop(signum, frame):
    print "Signal received, quitting after current iteration..."
    global Go
    Go = False

signal(SIGUSR1, stop)

c_file = 'work/combiner_{0}'
m_file = 'work/master_{0}'

def empty_combiner(meta):
    rmargs[-1] = meta
    call(rmargs)
    call(['truncate', '-s', str(filesize), meta])
    print "created final file",meta,"of size",filesize,"("+di+"x"+di+")"

#  thread target
def combine(tid, consumed, scratch=False):
    meta = c_file.format(tid)
    if scratch or not path.isfile(meta):
        empty_combiner(meta)

    while True:
        j = add_queue.get()
        if j is None:
            break
        mfile = m_file.format(j)

        loc_args = args[:]
        loc_args[-2] = str(j)
        loc_args[-1] = meta
        print "thread",tid,"calling"," ".join(loc_args)
        rv = call(loc_args)
        if rv != 0:
            print "thread",tid,"error with",mfile,"at",datetime.datetime.now()
            print "thread",tid,"POSSIBLE META BLOCK CORRUPTION."
        else:
            print "thread",tid,"done combining",mfile,"at",datetime.datetime.now()
            consumed[j] = 1
            rmargs[-1] = mfile
            call(rmargs)
            print "thread",tid,"removed",mfile,"at",datetime.datetime.now()
            dump_dict(consumed)

if __name__ == '__main__':
    #  set up workers
    if raw_input("Start from scratch? (Y for yes):") == "Y":
        consumed = new_dict(n,b)
        threads = [Thread(target=combine, args=(i,consumed,True)) for i in xrange(workers)]
    else:
        consumed = load_dict(dfname)
        threads = [Thread(target=combine, args=(i,consumed)) for i in xrange(workers)]
    queued = consumed.copy()

    for t in threads:
        t.start()

    while Go:
        produced = load_dict(fname)
        pcount = sum(v for v in produced.itervalues())
        qcount = sum(v for v in queued.itervalues())
        print pcount,"blocks made,",qcount,"queued."

        if pcount == qcount:
            if qcount == 0:
                sleep(30)
            elif pcount < nblocks:
                sleep(30)
            else:
                break
        for j in xrange(0, n, b):
            if produced[j]:
                if queued[j]:
                    pass
                else:
                    print "queueing column",j,"at",datetime.datetime.now()
                    add_queue.put(j)
                    queued[j] = 1
        sleep(5)

    for i in xrange(workers):
        add_queue.put(None)

    print "all columns produced and queued, waiting on workers"
    for t in threads:
        t.join()

    print "Fin."
