#!/usr/bin/env python2

import itertools
from sys import argv,exit
import pickle
import datetime
from math import ceil
from os import path

#  where 'BEGIN' is?
prefix='./'

if len(argv) < 2:
    print "Usage",argv[0],"e [online-mode] [extra-gb]"
    exit()
elif len(argv) < 3:
    online = False
else:
    online = True

e = int(argv[1])
    
extragb = 0
if len(argv) > 3:
    extragb = float(argv[3])

def fname():
    return "checkpoint/cols_" + str(e)

def load_dict():
    print "Dictionary loaded from " + fname()
    with open(fname(), 'r+b') as f:
        return pickle.load(f)

#  seconds in a timedelta objects (this is builtin in python 2.7)
def tsecs(td):
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6

def time_from_file(fi):
    # file whose fist line was created via "date >"
    #with open(fi, 'r') as f:
    #    read_data = f.readline().strip()
    #f.close()

    #return datetime.datetime.strptime(read_data, "%a %b %d %H:%M:%S %Z %Y")

    t = path.getmtime(fi)
    return datetime.datetime.fromtimestamp(t)


then = time_from_file(prefix+'BEGIN'+'.'+str(e))

if online:
	now = datetime.datetime.now()
else:
	now = time_from_file(fname())

n = 3**e
b = 2**(e+1)

rowsize = b/4
cbsize=(b+64)*rowsize

produced = load_dict()
pcount = sum(v for v in produced.itervalues())

dim = ceil(float(n)/b)
cols = dim
fullsize = float(cbsize*cols)/(1024**3)
donesize = float(cbsize*pcount)/(1024**3)

def mbps(done, delta, egb=0):
	seconds = tsecs(delta)
	donesize = float(cbsize*done)/(1024**3)
	bits = (donesize+egb) * (1024**3) * 8
	mbps = (bits/seconds)/(1024*1024)
	print "{0} ({1} s)".format(delta, seconds)
	print "Mbps: {0}".format(mbps)

print "{0}/{1} colblocks produced ({2:0.2f}/{3:0.2f}GB) ({4:0.2f}%)".format(pcount, int(cols), donesize, fullsize, 100*pcount/cols)
if extragb:
	print "Reported Extra GB:",extragb
print "production time",
mbps(pcount, now-then, extragb)
