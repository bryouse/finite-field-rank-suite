#!/bin/bash

e=${1:?"I need e!"}

BEGIN="BEGIN.$e"
END="END.$e"
LOG="MASTER.LOG.$e"

PROG=${2-"col-manager"}
echo "PROG is $PROG"

read -p "Do you want to delete ALL LOGS first? (need Y for yes) "
[ "$REPLY" == "Y" ] && rm -f $BEGIN $END $LOG

date >> $BEGIN
date >> $LOG
python -u $PROG.py "$e" | tee -a $LOG
date >> $LOG
date >> $END
