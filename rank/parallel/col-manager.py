import Pyro4
import Queue
from sys import argv,exit
import pickle
import datetime
from math import ceil

from dimensions import getn, getb #needs parent dir in PYTHONPATH

if len(argv) < 2:
    print "Usage",argv[0],"e"
    exit()
else:
    e = int(argv[1])

pyrohost = "localhost"

stype = 'thread'
#stype = 'multiplex'

n = getn(e)
b = getb(e)
dim = ceil(float(n)/b)
ncols = int(dim)

def fname():
    return "checkpoint/cols_" + str(e)

def dump_dict(d):
    with open(fname(), 'w+b') as f:
        pickle.dump(d, f)

def new_dict():
    print "Creating new list"
    d = {}
    for j in xrange(0, n, b):
        d[j] = 0
    dump_dict(d)
    return d

def old_dict():
    print "Dictionary loaded from " + fname()
    with open(fname(), 'r+b') as f:
        return pickle.load(f)

class BlockDoler(object):
    def __init__(self, dae, first=False):
        self.work = Queue.Queue()
        self.exponent = e
        self.daemon = dae
        self.no_work = False
        self.all_done = False
        if first:
            self.created = new_dict()
        else:
            self.created = old_dict()

        self.pcount = sum(v for v in self.created.itervalues())

        self.pfin()
        self.queue_work()

    def queue_work(self):
        for j in xrange(0, n, b):
            if self.created[j]:
                pass
            else:
                self.work.put(j)

    def exp(self):
        return self.exponent

    def pstatus(self):
        return self.work.qsize()

    def put_work(self, item):
        self.work.put(item)

    def get_work(self):
        self.pfin()
        self.pcount = self.pcount + 1
        if self.no_work:
            return -1
        return self.work.get()

    def join_work(self):
        self.work.join()

    def work_done(self, item):
        self.created[item] = 1 # document block creation
        dump_dict(self.created)
        print "master_"+str(item),"ready at",datetime.datetime.now()
        self.work.task_done()

    def pfin(self):
        #print "It's",self.pcount,"PVERSUS",ncols
        if(self.pcount >= ncols):
            self.no_work = True

    def shutdown(self):
        print "Shutdown signal received."
        self.daemon.shutdown()

Pyro4.config.SERVERTYPE = stype # try to get more concurrencey by forcing less concurrency :)
daemon=Pyro4.Daemon(host=pyrohost) # make a Pyro daemon
ns=Pyro4.locateNS(pyrohost) # find the name server

if raw_input("Start work from scratch? (Y for yes)") == 'Y':
    blocker=BlockDoler(daemon, True)
else:
    blocker=BlockDoler(daemon)

uri=daemon.register(blocker) # register as a Pyro object
ns.register("block.doler", uri)  # register in the name server

print "Ready."
daemon.requestLoop() # event loop server; wait for calls

print "All done."
