from Pyro4 import Proxy,naming,errors
from subprocess import call
from sys import argv
from time import sleep
import datetime
from threading import Thread,BoundedSemaphore,Lock
from signal import signal,SIGUSR1

nshost="localhost"
nameserver=naming.locateNS(nshost, 9090)

e = 0 
while not e:
    try:
        uri=nameserver.lookup("block.doler")
        bd=Proxy(uri)
        e = bd.exp()
    except:
        print "Cannot connect."
        sleep(2)

workers = 8
if len(argv) > 1:
    workers = int(argv[1])

print "Using",workers,"workers"

bs = BoundedSemaphore(workers)
printlock = Lock()

exe = './omp-col'

#  way to build a colblock on your system
args = ['srun', '-p', 'all', '-N', '1', '-c', '48', exe, str(e), 0]
#args = ['srun', '-p', 'all', '-N', '1', '-c', '48', 'whoami']
#args = [exe, str(e), 0]

def tprint(s):
    printlock.acquire()
    print s
    printlock.release()

def do_work(col):
    try:
        loc_args = args[:]
        loc_args[-1] = str(col)
        tprint("Starting col "+str(col)+" at "+str(datetime.datetime.now()))
        rv = call(loc_args)
        if rv != 0:
            print "ERROR with",loc_args
        else:
            bd.work_done(col)
        tprint("Finished col "+str(col)+" at "+str(datetime.datetime.now()))
    except (KeyboardInterrupt, SystemExit) as e:  #anything goes wrong
        print e
    except Exception as e:
		print e

    bs.release() # up semaphore

print "Getting work..."

Go = True

def stop(signum, frame):
    print "Signal received, quitting after current iteration..."
    global Go
    Go = False

signal(SIGUSR1, stop)


while Go:
    try:
        col = bd.get_work()

        if col == -1:
            print "No more work."
            break

        bs.acquire() # down semaphore
        #do work
        Thread(target=do_work, args=(col,)).start()

    except (errors.ConnectionClosedError, errors.CommunicationError) as e:
        print "Connection closed."
        break

    except KeyboardInterrupt as k:
        print "Keyboard killed me."
        break

print "Fin."
