#!/usr/bin/env python2

from subprocess import call
from sys import argv, exit
from dimensions import getn, getb
from datetime import datetime

out_dir = './work/'
exe_dir = './'


if len(argv) < 2:
    print 'usage: {0} e'.format(argv[0])
    exit()

exe = exe_dir+'omp-col'
print "using",exe

#  BUILD PHASE

e = int(argv[1])

args = [exe, str(e), 0]
#threads = 48
#args = ['srun', '-p', 'all', '-N', '1', '-c', str(threads), './omp-col', str(e), 0]

n = getn(e)
b = getb(e)

print n, b

for j in xrange(0,n,b):
    args[-1] = str(j)
    print args,datetime.now()
    call(args)

#  ADD PHASE

exe = exe_dir+'add-two-cols'
print "using",exe

#  compute the size of a master block to create
bpr = b/4
blocksize = bpr * b
extra = bpr * 64
totalsize = blocksize + extra

def fname():
    return out_dir+"meta_"+str(e)

def empty_meta():
    f = fname()
    call(['truncate', '-s', str(totalsize), f])
    print "Created intermediate file",f

args = [exe, str(e), 0]

empty_meta()
for j in xrange(0,n,b):
	args[2] = j
	print args
	call(map(str, args))
