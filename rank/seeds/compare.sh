#!/bin/bash

echo "mat: size vs. squares [(size+1)/squares should be 2.0]"
for i in D2 D4 D6 D8 D10 D12 D14 D16; do
#for i in D*; do
	s=`head -1 $i`
	q=`tail -1 $i | fgrep -o 1 | wc -l`
	echo "$i: $s vs. $q $(bc <<< "scale=1; $(($s+1))/$q" )"
done


