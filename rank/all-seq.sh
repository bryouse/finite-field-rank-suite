#!/bin/bash

e="${1:-4}"

MDIR="work"

rm $MDIR/master_[0-9]* $MDIR/meta_$e
./build-sequential.py $e
time ./sliced-rank $e $MDIR/meta_$e
