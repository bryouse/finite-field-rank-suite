#ifndef __ECHELON_FORM_H
#define __ECHELON_FORM_H
/* Compute row or column echelon form of  matrix over a field or gcd domain
RowEchelonFormIn(A) // modify A to row echelon form, return A
RowEchelonFormIn(U, A) // modify A to row echelon form, set U so that UA = original A, return A
ColEchelonFormIn(A) // modify A to col echelon form, return A
ColEchelonFormIn(U, A) // modify A to col echelon form, set U so that AU = original A, return A

In row echelon form the nonzero rows preceed the zero rows.
The leading entries in the nonzero rows are called the pivots.  
The columns of the pivots are in increasing order.  
Each pivot is the gcd of the entries in it's column at or below it's pivot row.
The form is reduced if the elements in each pivot column above the pivots are canonical
remainders with respect to the pivot.

The pivots and their locations are unique.  The reduced form is unique.  The transform U is unique only if the form is reduced and A has full row rank.

Example:
A = (1 2 3 ) = UR = (1  0) (1 2 3) = U'R' = (1  3) (1 2 0)
    (4 8 11)        (4 -1) (0 0 1)          (4 -1) (0 0 1),
where R is in row echelon form and R' is in reduced row echelon form.


Over a field, the pivots are 1's and the canonical remainders are 0's.
For the column echelon form, switch the roles of 'row and 'column in the above discussion.
In a nonzero line the first nonzero is the gcd of the entries of the counter line.

*/
/** Put A in (unreduced) row echelon form and return rank A.
If C is given, it is set to contain the cols of the pivots, C[i] = pivot col of row i.
If cert is given, it is reduced if any pivot comes from the last cert rows or last cert columns.  In that case, the final value of cert is reduced to the number of final rows and columns of the original A that are free of pivots.
In other words the final cert rows are dependent on the preceeding ones and the same for columns.
*/

#include <stdio.h>

namespace LinBox {

template<class Matrix> // Matrix - BB inheriting from MatrixBase
size_t rowEchelonFormIn(Matrix& A, vector<size_t>* C = 0, int* cert = 0)
/* Matrix features used:
	Matrix::RowIterator (points to Row vector)
	rowBegin(), rowEnd(),
	rowdim(), field(),
	getEntry(),
	swapability of rows,
	features of A's vector space used.
	  smulin()
	  axpyin()
	  getEntry()
*/
{
	//size_t m = A.rowdim();
	size_t n = A.coldim();
	size_t thresh = cert ? n - *cert : n;
	typedef typename Matrix::Domain Field;
	typedef typename Matrix::SlicedWord SlicedWord;
	Field& F = A.domain();
	typename Field::Element e; F.init(e);

	//std::cerr << "N" << n << " CERT" << cert << " THRESH" << thresh << std::endl;

	Matrix sub;
	setbuf(stderr, NULL);

	if (C != 0) C->clear();
	size_t r, c = 0, i = 0; // r,c = row, col of pivot
	for (r = 0; r < n; ++r)
	{
		//  gauge progress
		if(!(r % 100)) { fprintf(stderr, "%zu\r", r); fflush(stderr); }

		// find next pivot
		size_t cc = c;
		for (   ; cc < thresh; ++cc)
		{
			for (i = r; i < thresh && F.isZero( A.getEntry(e, i, cc) ); ++i)
			;
			if (i < thresh) break;  // found it.
			// else next cc.
		}
		if (cc == thresh) {// restart search. Allow dipping into the certificate
			for (   ; c < n; ++c)
			{
				for (i = r; i < n && F.isZero( A.getEntry(e, i, c) ); ++i)
				;
				if (i < n) break;  // found it.
				// else next c.
			}
			if (c == (n-1) && r == (n-1)){
				std::cerr << "c: " << c << " r: " << r << ", reporting full rank, 0 cert." << std::endl;
				cert = 0;
				return n;
			}
			if (c == n) break; // A is now in echelon form and r is the rank.
			// else carry out elimination step on r,c pivot entry e.
			if (cert && c > thresh) { 
				std::cerr << "failure to certify, dipping into cert cols:";
				std::cerr << " with cert " << *cert << ", c(" << c << ") > thresh(" << thresh << ")" << std::endl;
				std::cerr << " cert: " << *cert << "->" << n-c << std::endl;
				thresh = c; *cert = n - thresh; 
			}
			if (C != NULL) C->push_back(c);

			// ensure pivot in r,c position
			if (cert && i > thresh) { 
				std::cerr << "failure to certify, dipping into cert rows:";
				std::cerr << " with cert " << *cert << ", i(" << i << ") > thresh(" << thresh << ")" << std::endl;
				std::cerr << " cert: " << *cert << "->" << n-i << std::endl;
				thresh = i; *cert = n - thresh; 
			}
		}
		else 
			c = cc;
		/* TODO  64 = SIZE */
		size_t skippedUnits = c/(sizeof(SlicedWord)*8);

		if (i > r) // swap these rows
		{
			//A._VS.swap(*Ar, *Ai);
			typename Matrix::RawIterator ip = A.rowBegin(i)+skippedUnits, rp = A.rowBegin(r)+skippedUnits;
			for( ; ip != A.rowEnd(i); ++ip, ++rp)  swap(*ip, *rp);
		}
		//std::cerr << "i" << i << " and r" << r << std::endl;
		//A.write(std::cout);

		//A._VS.smulin(*Ar, e); // fix later: replace *Ar with vector starting at col c.
		sub.submatrix(A, r, 0, 1, A.coldim());
		sub.smulin(e);
		//std::cerr << "smulling e" << e << " and r" << r << std::endl;
		//A.write(std::cout);

		typename Matrix::RawIterator ip, rp;
		//  looping through rows, good candidate for openMP parallelization.
		#pragma omp parallel for private(ip, rp, e)
		for (i = r+1; i < n; ++i)
		{
			A.getEntry(e, i, c);
			F.negin(e); // e = -a_i,c
			//A.axpyin(A.rowBegin(i), A.rowEnd(i), e, A.rowBegin(r)); // fix later to start at col c.
			ip = A.rowBegin(i)+skippedUnits;
			rp = A.rowBegin(r)+skippedUnits;
			switch(static_cast<int>(e)){
				case 0: break; 
				case 1: for( ; ip != A.rowEnd(i); ++ip, ++rp) (*ip) += (*rp); 
						break;
				case 2: for( ; ip != A.rowEnd(i); ++ip, ++rp) (*ip) += (*rp)*2; 
						break;
				default: std::cerr << "something amiss in e=" << static_cast<int>(e) << std::endl;
			}
			/*
			for( ; ip != A.rowEnd(i); ++ip, ++rp){
				switch(static_cast<int>(e)){
					case 0: break; 
					case 1: (*ip) += (*rp); break;
					case 2: (*ip) += (*rp) * 2; break;
				}
			*/
			//std::cerr << "axpy'd e" << e << " into i" << i << " from r" << r << std::endl;
			//A.write(std::cout);
		}
	}
	return r; // == n, full row rank case.
}

} // LinBox

#endif // __ECHELON_FORM_H
