#ifndef _COMMON_H
#define _COMMON_H

#include <cstddef>  // to get around a GMP bug with c++11
#include <sstream>
#include <string>

//  M
#define COMMON_M expt(3,e)

//  B
#define COMMON_B expt(2,e+1) // DICKSON (small)
//#define COMMON_B expt(2,e+1)+expt(2,e-2)  // DICKSON (big)
//#define COMMON_B expt(2, e+3) //  DING-YUAN
//#define COMMON_B expt(2,e+2) // C-G (small)
//#define COMMON_B expt(2,e+2)+expt(2,e-1) // C-G (big)

// D
#define COMMON_D b  

#define FILEPREFIX "./"
#define OUTFILEPREFIX "work/"
#define SPREFIX "seeds/"

#define MPREFIX "master_"
#define TEMPREFIX "Tmaster_"
#define METAPREFIX "meta_"

const std::string slfile(size_t i, size_t j){
	std::stringstream ss;
	ss << OUTFILEPREFIX << j << "_" << i;

	return ss.str();
}

const std::string mfile(size_t j){
	std::stringstream ss;
	ss << OUTFILEPREFIX << MPREFIX << j;

	return ss.str();
}

const std::string tempfile(size_t j){
	std::stringstream ss;
	ss << OUTFILEPREFIX << TEMPREFIX << j;

	return ss.str();
}

const std::string metafile(size_t e){
	std::stringstream ss;
	ss << OUTFILEPREFIX << METAPREFIX << e;

	return ss.str();
}

const std::string seedfile(size_t e){
	// squares file in format "De" for numeric exponent e
	std::stringstream ss;
	ss << FILEPREFIX << SPREFIX << "D" << e;

	return ss.str();
}

size_t expt(size_t a, size_t n) { return n == 0 ? 1 : a*expt(a, n-1); }

#endif
