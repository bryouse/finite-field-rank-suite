

/* linbox/blackbox/sssrg-adjJIT.h
 * Copyright (C) 2012, B. David Saunders
 *
 * evolved from dixonsrgJIT.h Written by John P. May <jpmay@cis.udel.edu>
 *
 * LinBox is free software. See topdir/COPYING* for license (LGPL v2.1).
 */
/**
MatrixDomain MD(3); 
SSS3RG_JIT A(MD, e, file);
MD::Matrix B(MD, b, b);
A.submatrix(B, i, j); // fill B from A, starting at i,j position of A

**/

#ifndef LINBOX_SSS3RG_JIT_H_
#define LINBOX_SSS3RG_JIT_H_

#include <vector>

#include "sliced3e-additive-group.h"

#ifdef STEPPER
#ifdef FILESTEP
#include "file-stepper.h"
#else
#include "sliced-stepper.h"
#endif
#endif

#include <cstdlib>

namespace LinBox {

class AbelianGroup;

template<class _MatrixDomain> // must be a representation of GF(3).
class SSS3RG_JIT{ // Semifield Squares Strongly 3 Regular Graph Just-In-Time (Adjacency) Matrix
public:
	typedef _MatrixDomain MatrixDomain;

	// MatrixDomain's Matrix is some flavor of Dense matrix
	typedef typename MatrixDomain::Matrix Matrix; 
	typedef typename MatrixDomain::Element Scalar; // field element (GF(3))

	typedef Sliced3eAdditiveGroup SemiField; // using sub, init, convert, incr.

	typedef typename SemiField::Element Element;

protected:
	MatrixDomain MD_; // Over prime field (GF(3)).  // note: MD_ is not used, but MatrixDomain:: types are used.
	SemiField K_; 
		
	size_t N_; // size of semifield and of matrix; For _N_ = q^2, 
				// semifield is GF(q)^2, with a nonassoc mul.
	Scalar* D_; // Membership table for the Paley Type PDS D.
				// Entry _D[k] = 1 iff _K.convert(x, k) is a square.
		
public:
//					SSS3RG_JIT(MD, e, file);
					SSS3RG_JIT(): D_(0) {};
					~SSS3RG_JIT() { delete(D_); }
	MatrixDomain&	domain() { return MD_; }
	size_t			rowdim() { return N_; }
	size_t			coldim() { return N_; }
//	Scalar &		getEntry(x, i, j);
//	SSSRG_Mat&	submatrix(B, i, j);

// implementations
		
	// construct allocating full matrix
	SSS3RG_JIT(MatrixDomain& MD, size_t e, string file)
	: MD_(MD), K_(MD.characteristic(), e), D_(0) 
	{ init(file); } 

	void init(string D) // read the D_ table of squares
	{
		ifstream data(D.c_str());
		
		string s;
		do { getline(data, s); } while (s.size() > 0 && s[0] == '%');
		stringstream ss(s);
		ss >> N_;
		if(K_.cardinality() != N_) {
			std::cerr << "Warning, size disagreement: file " << N_ << ", semifield " << K_.cardinality() << std::endl;
		    N_ = K_.cardinality();
		}
		// build _D
		D_ = new Scalar[N_];
		//for (size_t i = data.get(); i < '0' || i > '9'; i = data.get());
		for (size_t i = 0; i < N_; ++i){  D_[i] = data.get()-'0'; }
		//for (size_t i = 0; i < N_; ++i) data >> D_[i];
		D_[0] = 2;
		
		//MD_.init(D_[0],p_-1);
	}// constructor

	inline Scalar & getEntry(Scalar & x, size_t i, size_t j) { 
		// Return 1 if j - i is a square, (p-1 if i == j), zero ow.
		SemiField::Element xi, xj, xk;
		size_t k;
		K_.init(xi, i); K_.init(xj, j); K_.init(xk, 0);
		K_.sub(xk, xj, xi);
		K_.convert(k, xk);
		return x = D_[k];
	}

#ifndef STEPPER
	struct stepper // dummy class for using setEntry instead
	{ stepper (Matrix& A) {}
	  stepper (size_t a, size_t b) {}
	  void row() const {} };
#endif

	// Write to A a block starting at i0, j0 of me. Size of block is that of A.
	Matrix& getSubmatrix(Matrix& A, size_t i0, size_t j0) {
		SemiField::Element ie, je, ke;
		size_t k;
#ifdef FILESTEP
		stepper S(i0, j0);
#else
		stepper S(A);
#endif
		K_.init(ie, i0); K_.init(ke);
		for (size_t i = 0; i < A.rowdim(); i++) {
			K_.init(je, j0); // lookup? (not inner loop)
			for (size_t j = 0; j < A.coldim(); j++) {
				K_.sub(ke, ie, je); // componentwise
				K_.convert(k, ke); // lookup?
#ifdef STEPPER
				S.step(D_[k]);
#else
				A.setEntry(i, j, D_[k]);
#endif
				K_.incr(je); // lookup? 
			}
			S.row();
			K_.incr(ie);
		}
		return A; // may be artificial, with stepper writing to file.

	}

	// optional
	//Matrix& submatrix(Matrix& A, size_t i0, size_t j0, size_t r, size-t c) 
	//{  A.resize(r, c); return getSubmatrix(A, i0, j0); }
	// needed
	//Matrix& writeSubmatrix(size_t i0, size_t j0, size_t r, size-t c) ;


}; // class SSS3RG_JIT

} // LinBox
#endif // LINBOX_SSS3RG_JIT_H_
