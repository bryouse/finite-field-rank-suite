#ifndef SLICED3EADDITIVEGROUP_H_
#define SLICED3EADDITIVEGROUP_H_

#include <vector>
using namespace std;
#include "linbox/util/error.h"
#include "dense-sliced.h"
/*
   An additive group has the additive portion of the ring/field interface
   It also has incr() and careful handling of init() and convert() for the sake of 
   an effective mapping of elements to indices.
*/

namespace LinBox {

#define S3EAG_SIZE 32

/**

Sliced3eAdditiveGroup provides the additive portion of GF(3,e), 0 < e <= 16,
representing elements as polynomials of degree < e, stored in a sliced unit, with the 
i-th coefficient (element of Z_3) in the i-th bit position of the 16 bit sliced unit.
   
  */
struct Sliced3eAdditiveGroup /* : public AdditiveGroup */ { 
	typedef uint32_t Word;
	typedef SlicedBase<Word> Element;
	size_t p_; // = 3 
	size_t e_; // exponent, length of Element in p-digits
	size_t card_;
	size_t pow2e_;
	//vector<size_t> firstZero;
	vector<size_t> powSum;


	Sliced3eAdditiveGroup(): p_(3), e_(0), pow2e_(1<<e_) {}
	void group_init(size_t e) { 
		if (e > S3EAG_SIZE) {throw LinboxError("LinBox Exception in Sliced3eAdditiveGroup: max exponent"); }
		e_ = e; pow2e_ = 1<<e; 
	}

	Sliced3eAdditiveGroup(size_t p, size_t e): p_(p), e_(e), pow2e_(1<<e) {
		if (e > S3EAG_SIZE) {throw LinboxError("LinBox Exception in Sliced3eAdditiveGroup: max exponent"); }
		card_ = 1;
		for (size_t i = 0; i < e_; ++i) card_ *= p_; // card = p^e

		// powSum[i] is the value the bit sequence of i would represent if the base were 3 instead of 2.
		// It is used in convert().
		powSum.resize(pow2e_, 0);
		for (size_t i = 0; i < pow2e_; ++i) {
			size_t foo = 0;
			for (size_t j = pow2e_; j > 1; ) {
				j >>= 1;
				foo = (foo*p_) + ((i&j) ? 1 : 0);
			}
			powSum[i] = foo;
			//std::cout << "in cstor, i, powSum[i]: " << i << " " << powSum[i] << std::endl;
		}

	}

	// Example:  i = 33 (representing 27 + 6, ie z^3 +2z) goes to x.b0 = 1010, x.b1 = 0010)
	Element& init(Element& x, size_t i = 0){
		x.zero(); // feature of sliced
		for (size_t k = 0; k < e_; ++k){
			//x.setEntry(k, i%p_), i /= p_;
			Word c = i%p_;  // coefficient c
			i /= p_; // remaining coefs
			// c is 0, 1, or 2.
			x.b0 |= (c&1) << k; // getting 1 
			c = (c >> 1) << k; // if set, c was a 2.
			x.b0 |= c; // getting half of 2 
			x.b1 |= c; // getting other half of 2 
		}
		return x;
		/*
		   size_t k = e/2;  if (k + k < e) k += 1; 
		   mid = 3^k;
		   L = (1 << k) - 1; // mask to select lowest k bits.
		   H = ((1 << e) - 1) & !L; // mask to select remaining bits
		   vector<Word> slice(mid_);
		   for (int i = 0; i < mid; ++i) {
		  		slice[i] = ?? extract nonzero 3-digits and 2 3-digits.
		   }
		   // class vars above.
		   size_t il = i % mid, ih = i/mid;
		   i0 = slice[il]; i1 = slice[ih]; // slice contains k b0 bits then k b1 bits
		   x.b0 = (i0 & L) | ((i1 & L) >> k);

		   x.b1 = ((i0 & H) << k) | (i1 & H);
		*/
	}

	size_t characteristic() { return p_; }
	size_t cardinality() { return card_; }

	void incr(Element& x){
		// a table lookup would speed this
		Word mask = 1;
		while (x.b1 & mask){ // leading 2's --> 0's
			x.b1 ^= mask; // turn it off
			x.b0 ^= mask; // turn it off
			mask <<= 1;
		}
		// incr a 0 or 1.
		if (mask < pow2e_) {
			if (x.b0 & mask) x.b1 |= mask; // 1 --> 2
			else x.b0 |= mask; // 0 --> 1
		}
		return; 
	}

	// Example:  x.b0 = 1010, x.b1 = 0010) goes to i = 33 (representing z^3 +2z).
	// powSum[b0] is 30, powSum[b1] is 3.
	size_t& convert(size_t& i, Element& x){
		i = powSum[x.b0] + powSum[x.b1]; // 1010,0010 -> 30 + 3.
		return i;
	}

	Element& sub(Element& c, const Element& a, const Element& b) {
		c = b*2; // negate, c <- -b
		return c += a; // c = -b + a.
	}

	Element& neg(Element& b, const Element& a) {
		Element z; init(z, 0);
		return sub(b, z, a);
	}

	Element& add(Element& c, const Element& a, const Element& b) {
		Element mb; init(mb, 0);
		return sub(c, a, neg(mb, b));
	}

}; // Sliced3eAdditiveGroup

}//LinBox Namespace

#endif // SLICED3EADDITIVEGROUP_H_
