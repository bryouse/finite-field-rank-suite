# README #

This is the complementary code to my Ph.D. thesis:  High Performance Exact Linear Algebra.

### Structure ###

* `rep/` - codes for fast/efficient compression of small-prime finite fields
* `rank/` - codes for computing ranks of the Dickson SRG adjacency matrices
* `tests/` - main functions to test and compare compression schemes

### How to ###

* Drop this repository into your LinBox install's examples/ subdirectory
*Much of this work is being included in LinBox directly-- or already is).  So this isn't a proper software package.  Thus, simply hand-edit `lb_install` to point to your LinBox installation directory, so the makefiles are happy.
* Call `make` from the top level to build the executables
* Go build and compute the rank on a Dickson matrix at blazing speeds:
    * From within `rank/`, try `./all-seq.sh e` where `e` is a positive even integer.  This
        * Builds a compressed Dickson matrix sequentially (better limit `e` to 10 or 12)
        * Computes the rank of the compressed matrix using parallel Gaussian elimination
    * See the `parallel` directory for the framework used to build the Dickson matrices in... parallel